# Smart-Blind-Navigation-with-Arduino-Sonar-Sensor-and-Motor-control

It's a solar powered, low cost and light weight navigation device for blind people which will be worn in hand. As soon as blind people approach any obstacle, the device starts to give signal by increasing or decreasing motor vibration.

  1. It's a low cost and light weight navigation device for blind people which is worn in hand.<br>
  2. As soon as a blind people approach any obstacle, the device will start to give signal by increasing or decreasing motor vibration and the blind person will feel the vibration in hand accordingly.<br>
  3. One needs a sonar sensor, an arduino uno and a vibrating motor to run the project.<br>
  4. Please see the code to connect to the defined pwm pins with output vibrating motor.<br>

     ![](Image/Image.png)
  5. If you want to see the publication, please visit: https://www.researchgate.net/publication/333370706<br />
  6. If you want to cite the research paper, please add these lines to the reference section of your research paper:<br />
`    A. Chatterjee, S. Dutta, P. Sarkar and A. B. M. A. A. Islam, "Obstacle Detector for Blind
     People with Low Cost (Poster Presentation)," Proceedings of 2017 International
     Conference on Networking, Systems and Security (NSysS), Dhaka, 2017`<br />
  7. If you want to cite the code, add the DOI number to the reference section of your research paper: 
     [![DOI](https://zenodo.org/badge/224455529.svg)](https://zenodo.org/badge/latestdoi/224455529)